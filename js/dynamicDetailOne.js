var APIHOST = "http://wechat.y-med.com.cn/";
var PIC_PATH = APIHOST + "attachment/";
let id = getQueryString('id')
let atype = getQueryString('atype')
$(function () {
    getDynamic();
});
//获取大会动态
function getDynamic() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_article",
        data: { atype: atype,id:id },
        success: function (result) {
            if (result.code == 100 && result.data.length > 0) {
                var data = result.data[0];

                $('.Title').html(data.title);
                $('.Time').html('时间：'+data.addtime);
                $('.contents').html(data.content);
            } else if (result.code == -100) {
                $('.Title').html('404，当前页面丢失');
                setTimeout(() => {
                    window.close();
                }, 3000)
            }
        }
    });
}



//获取地址参数
function getQueryString(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  unescape(r[2]); return null;
}