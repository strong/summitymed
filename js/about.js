var APIHOST = "http://wechat.y-med.com.cn/";
var PIC_PATH = APIHOST + "attachment/";

$(function () {
    getDynamic();
});
//获取前沿十大项目
function getDynamic() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_article",
        data: { atype: 11 },
        success: function (result) {
            var dynamic_html = "";
            if (result.code == 100 && result.data.length > 0) {
                $.each(result.data, function (k, v) {//这里的函数参数是键值对的形式，k代表键名，v代表值
                    dynamic_html += ''
                    +"<a class='projectItem' target='_blank' href='./dynamicDetailOne.html?id="+v.id+"&atype="+v.atype+"'>"
                        +"<img src='" + PIC_PATH + v.img + "' alt='' />"
                        +"<p class='projectTitle'>"+v.title+"</p>"
                    +"</a>";
                });
                $('.projectBox').html(dynamic_html);
            }
        }
    });
}
