var APIHOST = "http://wechat.y-med.com.cn/";
var PIC_PATH = APIHOST + "attachment/";

$(function () {
    getLeader();
    getHospitalLeader();
    getKexue();
});
//获取协会领导
function getLeader() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_leader",
        data: { type: 1 },
        success: function (result) {
            var leader_html = "";
            var leadermore_html = '';
            if (result.code == 100 && result.data.length > 0) {

                $.each(result.data, function (k, v) {//这里的函数参数是键值对的形式，k代表键名，v代表值
                    leader_html += ''
                        + "<div class='MemberItem'>"
                            + "<dl>"
                                + " <dt>"
                                    + " <img class='peopleA' src='" + PIC_PATH + v.timg + "' alt=''"
                                + "</dt>"
                                + "<dd>"
                                    + "<span class='MemberItemName'>" + v.tname + "</span>"
                                    + "<span class='MemberItemLocal'>" + v.association + "</span>"
                                    + " <span class='MemberItemPosition'>" + v.zhiwu + "</span>"
                                + "</dd>"
                            + "</dl>"
                        + "</div>";
                });
                leadermore_html += leader_html+'<div class="MemberItem"><a target="_blank" href="./leader.html" class="more">了解更多</a></div>'
                $('#MemberList').html(leader_html);
                $('#MemberListMore').html(leadermore_html);

            }
        }
    });
}
//获取医院领导
function getHospitalLeader() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_leader",
        data: { type: 2 },
        success: function (result) {
            var leader_html = "";
            var hostpitalList = [];

            if (result.code == 100 && result.data.length > 0) {
                hostpitalList = result.data;
                hostpitalList.push({ "hospital": "后续名单持续确认中", "tname": "" });
                $.each(result.data, function (k, v) {//这里的函数参数是键值对的形式，k代表键名，v代表值
                    if (k == 0) {
                        leader_html += " <tr>";
                    }
                    if (k == hostpitalList.length - 1) {
                        leader_html += "<td class='hospitalLast'>" + v.hospital + " " + v.tname + "</td>";
                    } else {
                        leader_html += "<td>" + v.hospital + "" + v.tname + "</td>";
                    }

                    if ((k + 1) % 3 == 0) {
                        leader_html += " </tr>";
                    }
                });
                $("#HostpitalList").html(leader_html);

            }
        }
    });
}
    //获取科学领导
function getKexue() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_leader",
        data: { type: 3 },
        success: function (result) {
            var leader_html = "";
            var leader_right = "";
            var leadermore_html = '';
            if (result.code == 100 && result.data.length > 0) {

                $.each(result.data, function (k, v) {//这里的函数参数是键值对的形式，k代表键名，v代表值
                    if(k % 2 == 0){
                        leader_right += ''
                        + "<tr>"
                        + "<td class='one'>"+v.association+"</td>"
                        + "<td class='two'>"+v.zhiwu+"</td>"
                        + "<td class='three'>"+v.tname+"</td>"
                        + "<td class='four'>"+v.hospital+"</td>"
                        + "</tr>"
                    }else{
                        leader_html += ''
                        + "<tr>"
                        + "<td class='one'>"+v.association+"</td>"
                        + "<td class='two'>"+v.zhiwu+"</td>"
                        + "<td class='three'>"+v.tname+"</td>"
                        + "<td class='four'>"+v.hospital+"</td>"
                        + "</tr>"
                    }
                    
                });
                leadermore_html += leader_html+'<div class="MemberItem"><a target="_blank" href="./leader.html" class="more">了解更多</a></div>'
                $('.departLeft').html(leader_html);
                $('.departRight').html(leader_right);

            }
        }
    });

}