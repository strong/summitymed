var APIHOST = "http://wechat.y-med.com.cn/";
var PIC_PATH = APIHOST + "attachment/";

$(function () {
    getDynamic();
});
//获取大会动态
function getDynamic() {
    $.ajax({
        url: APIHOST + "wapi/public/index.php/cmea/index/get_article",
        data: { atype: 12 },
        success: function (result) {
            var dynamic_html = "";
            if (result.code == 100 && result.data.length > 0) {

                $.each(result.data, function (k, v) {//这里的函数参数是键值对的形式，k代表键名，v代表值
                    
                    
                    // <span class="container_Item">
                    //     <span>天津市发布卒中急救地图 提升脑卒中患者救治效率</span>
                    //     <span>脑卒中具有发病率高、致残率高、死亡率高、复发率高、医疗费高的特点，严重威胁人民群众的生命健康。“时间就是大脑”，愈早识别、愈早送医、愈早救治，对急性脑卒中患者后期的恢复效果、对减轻社会家庭负担十分重要。打通这条生命</span>
                    //     <span>2019-03-29</span>
                    // </span>
                
                    dynamic_html += ''
                        + "<a class='container_list' target='_blank' href='./dynamicDetailOne.html?id="+v.id+"&atype="+v.atype+"'>"
                           
                                + " <span class='container_img'>"
                                    + " <img src='" + PIC_PATH + v.img + "' alt=''"+"/>"
                                + "</span>"
                                + "<span class='container_Item'>"
                                    + "<span>" + v.title + "</span>"
                                    + "<span>" + v.content + "</span>"
                                    + " <span>" + v.addtime + "</span>"
                                + "</span>"
                        + "</a>";
                });
                $('.boxCenter').html(dynamic_html);

            }
        }
    });
}
